﻿using Contact.View;
using Contact.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Contact
{
    public partial class Login : ContentPage
    {
        
        public Login()
        {
            //NavigationPage.SetHasBackButton(this, false);
            //NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            // btnSignUp.Clicked += BtnSignUp_Clicked;
            //btnLogin.Clicked +=  BtnLogin_Clicked;
            btnLogin.Clicked += BtnLogin_Clicked;
            // loginActivity.SetValue(IsVisibleProperty, false);
            BindingContext = new RegisterViewModel();

        }

        private async void  BtnLogin_Clicked(object sender, EventArgs e)
        {
           
            try
            {
                if (etrUsername.Text == null || etrUsername.Text == "")
                {
                    lblMessage.Text = "Please enter your username";
                }
                else if (etrPassword.Text == null || etrPassword.Text == "")
                {
                    lblMessage.Text = "Please enter your password";
                }
                else
                {
                    //loginActivity.IsRunning = true;
                    //loginActivity.SetValue(IsVisibleProperty, true);
                    IsBusy = true;
                    lblMessage.Text = "";
                    await Task.Delay(400);
                    // loginActivity.IsRunning = false;
                    IsBusy = false;
                    await Navigation.PushAsync(new ListContacts());

                    //Navigation.InsertPageBefore(new ListContacts(), this);
                    //await Navigation.PopAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
        }

        private void BtnSignUp_Clicked(object sender, EventArgs e)
        {
            //  throw new NotImplementedException();

         //   Navigation.PushAsync(new SignUp());
          //  Navigation.PushAsync(new Register());
        }
    }
}
