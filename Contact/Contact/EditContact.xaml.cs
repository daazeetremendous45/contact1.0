﻿using Contact.DAL;
using Contact.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Contact
{
    public partial class EditContact : ContentPage
    {
        ContactDA NewContactDA = new ContactDA();
        Contacts ContactsObj = new Contacts();
        string ContactId;
        public EditContact(string PhoneNumber, string Name, string Id)
        {
            InitializeComponent();
            etrName.Text = Name;
            etrPhoneNo.Text = PhoneNumber;
            ContactId = Id;
            Title = Name;
        }

        private void SaveContact(object sender, EventArgs e)
        {
            ContactsObj.FullName = etrName.Text;
            ContactsObj.PhoneNumber = etrPhoneNo.Text;
            ContactsObj.ContactID =Convert.ToInt32(ContactId);
            NewContactDA.Update(ContactsObj);
            Navigation.PushAsync(new ListContacts());


        }
    }
}
