﻿using Contact.DAL;
using Contact.Model;
using Contact.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Contact
{
    public partial class ListContacts : ContentPage
    {
        ContactDA NewContactDA = new ContactDA();
        Contacts ContactObj = new Contacts();
        public ListContacts()
        {
            InitializeComponent();
             Title =  "Contact List";
            Icon = null;
            //var ContactList = new ListView();
            ContactList.ItemsSource = NewContactDA.ListAllContact();
            //To change the default color
            var cell = new DataTemplate(typeof(TextCell));
           // cell.SetBinding(TextCell.TextProperty, new Binding(".")); //Binds directly to the namesace
            cell.SetBinding(TextCell.TextProperty, new Binding("FullName"));
            cell.SetBinding(TextCell.DetailProperty, new Binding("PhoneNumber"));
            cell.SetValue(TextCell.TextColorProperty, Color.Black);
            cell.SetValue(TextCell.DetailColorProperty, Color.Black);
            ContactList.ItemTemplate = cell;
            //Color change ends here
             Content = ContactList;
           NavigationPage.SetHasBackButton(this, false);
            

            ContactList.ItemSelected += (sender, e) => {
                //e.SelectedItem
                //Debug.WriteLIne("You selected" + e.SelectedItem);
                if (e.SelectedItem != null)
                {
                    string SelectedNo= e.SelectedItem.ToString();
                    Navigation.PushAsync(new ManageContact(SelectedNo));
                    //Remove the highlight bar
                    ContactList.SelectedItem = null;
                }

            };

        }

        private void OnAddClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddContact());
        }


        
    }
}
