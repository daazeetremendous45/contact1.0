﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contact.Model
{
    [Table("Contacts")]
    public class Contacts
    {
        [PrimaryKey, AutoIncrement]
        public int ContactID { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public override string ToString()
        {
            return PhoneNumber;
        }
    }

    
}
