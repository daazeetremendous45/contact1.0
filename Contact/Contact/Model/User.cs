﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Contact.Model
{
    [Table("User")]
    public class User
    {
        [PrimaryKey, AutoIncrement]
        private int userId { get; set; }
        private string username;
        private string password;


        public int UserId
        {

            get { return userId; }
            set
            {
                userId = value;

            }

        }

        public string Username
        {

            get { return username; }
            set
            {
                username = value;

            }

        }

        public string Password
        {

            get { return password; }
            set
            {
                password = value;
            }



        }
    }
}
