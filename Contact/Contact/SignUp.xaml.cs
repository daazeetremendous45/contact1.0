﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Contact
{
    public partial class SignUp : ContentPage
    {
        public SignUp()
        {
            InitializeComponent();

            var sex = new[]
           {
                "Select Sex",
                "Male",
                "Female"
            };

            Sex.ItemsSource = sex;
        }
    }
}
