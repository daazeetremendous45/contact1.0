﻿using Contact.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Contact
{
    public partial class ManageContact : ContentPage
    {
        ContactDA NewContactDA = new ContactDA();
        string PhoneNumber, Name, Id;
        public ManageContact(string SelectedNumber)
        {
            InitializeComponent();
            var result= NewContactDA.GetContactByPhoneNumber(SelectedNumber);
            Title = Name = result.FullName;
            lblPhoneNumber.Text = PhoneNumber= result.PhoneNumber;
            lblContactId.Text = Id= result.ContactID.ToString();
            //lblContactId.SetBinding((IsVisibleProperty, "False");
            lblContactId.SetValue(IsVisibleProperty, false);
            //lblContactId.
        }

        private void OnEditClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EditContact(PhoneNumber,  Name,  Id));
        }

        private async void OnDeleteClick(object sender, EventArgs e)
        {
          var result= await  DisplayAlert("Confirm Delete", "Are you sure you want to delete", "OK", "Cancel");
            if (result.ToString()=="True")
            {
                NewContactDA.DeleteCustomer(Convert.ToInt32(Id));
               await Navigation.PushAsync(new ListContacts());
            }
        }
    }
}
