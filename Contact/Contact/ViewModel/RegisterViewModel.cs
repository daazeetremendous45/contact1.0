﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using Xamarin.Forms;
using Contact.Model;
using Contact.DAL;

namespace Contact.ViewModel
{
    public class RegisterViewModel : INotifyPropertyChanged
    {
        public RegisterViewModel()
        {
            SignUp = new Command(async () => await RegisterUser(), () => !IsBusy);
        }
        private int userId { get; set; }
        private string username;
        private string password;
        private string confirmPassword;
        private bool isBusy;
        private bool signUpEnabled = false;
        private bool messageIsVisible = false;
        public string message { get; set; }

        UserDA NewUserDA = new UserDA();
        User UserObj = new User();

        public Command SignUp { get; }


        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public int UserId
        {

            get { return userId; }
            set
            {
                userId = value;
                OnPropertyChanged(nameof(UserId));
            }

        }

        public string Username
        {

            get { return username; }
            set
            {
                username = value;

                if (username != null && Password != null && ConfirmPassword != null)
                {
                    SignUpEnabled = true;
                }
                else
                {
                    SignUpEnabled = false;
                }

                //Clear Message on Screen
                if (username != null || Password != null || ConfirmPassword != null)
                {
                    Message = "";
                    MessageIsVisible = false;
                }
                OnPropertyChanged(nameof(Username));
            }

        }

        public string Password
        {

            get { return password; }
            set
            {
                password = value;
                if (Username != null && password != null && ConfirmPassword != null)
                {
                    SignUpEnabled = true;
                }
                else
                {
                    SignUpEnabled = false;
                }
                //Clear Message on Screen
                if (Username != null || password != null || ConfirmPassword != null)
                {
                    Message = "";
                    MessageIsVisible = false;
                }
                OnPropertyChanged(nameof(Password));
            }

        }

        public string ConfirmPassword
        {

            get { return confirmPassword; }
            set
            {
                confirmPassword = value;
                if (Username != null && Password != null && confirmPassword != null)
                {
                    SignUpEnabled = true;
                }
                else
                {
                    SignUpEnabled = false;
                }
                //Clear Message on Screen
                if (Username != null || Password != null || confirmPassword != null)
                {
                    Message = "";
                    MessageIsVisible = false;
                }
                OnPropertyChanged(nameof(ConfirmPassword));
            }

        }

        public string Message
        {

            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged();
            }

        }


        public bool IsBusy
        {

            get { return isBusy; }
            set
            {
                isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
                SignUp.ChangeCanExecute();
            }

        }

        public bool SignUpEnabled
        {

            get { return signUpEnabled; }
            set
            {
                signUpEnabled = value;
                OnPropertyChanged(nameof(SignUpEnabled));

            }

        }
        //Register new user

        public bool MessageIsVisible
        {

            get { return messageIsVisible; }
            set
            {
                messageIsVisible = value;
                OnPropertyChanged();
            }

        }
        async Task RegisterUser()
        {
            if (Password != ConfirmPassword)
            {
                Message = "Password does not matches";
                MessageIsVisible = true;
            }
            else if (NewUserDA.GetUserByUsername(Username) != null)
            {
                Message = "Username already exist";
                MessageIsVisible = true;
            }
            else
            {
                IsBusy = true;
                UserObj.Username = Username;
                UserObj.Password = Password;
                int result = NewUserDA.Insert(UserObj);

                await Task.Delay(4000);

                IsBusy = false;
                Message = "Record save successfully";
                MessageIsVisible = true;
            }
        }
    }
}
