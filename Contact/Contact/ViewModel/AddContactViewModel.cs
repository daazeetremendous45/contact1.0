﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using Xamarin.Forms;
using Contact.Model;
using Contact.DAL;

namespace Contact.ViewModel
{
    public class AddContactViewModel : INotifyPropertyChanged
    {
        private INavigation _navigation;
        private ContactDA NewContactDA = new ContactDA();
        private Contacts ContactsObj = new Contacts();
        public AddContactViewModel(INavigation navigation)
        {
            SaveContact = new Command(async () => await AddContact());
            _navigation = navigation;
        }

        private string name;
        private string phoneNo;
        private bool isBusy;
        private bool saveContactEnabled = false;
        private bool messageIsVisible = false;
        public string message { get; set; }
        public Command SaveContact { get; }


        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string Name
        {

            get { return name; }
            set
            {
                name = value;

                if (name != null && PhoneNo != null)
                {
                    SaveContactEnabled = true;
                }
                else
                {
                    SaveContactEnabled = false;
                }

                //Clear Message on Screen
                if (name != null || PhoneNo != null)
                {
                    Message = "";
                    MessageIsVisible = false;
                }
                OnPropertyChanged();
            }

        }

        public string PhoneNo
        {

            get { return phoneNo; }
            set
            {
                phoneNo = value;
                if (Name != null && phoneNo != null)
                {
                    SaveContactEnabled = true;
                }
                else
                {
                    SaveContactEnabled = false;
                }
                if (name != null || PhoneNo != null)
                {
                    Message = "";
                    MessageIsVisible = false;
                }
                OnPropertyChanged();
            }

        }

        public string Message
        {

            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged();
            }

        }


        public bool IsBusy
        {

            get { return isBusy; }
            set
            {
                isBusy = value;
                OnPropertyChanged();
                SaveContact.ChangeCanExecute();
            }

        }

        public bool SaveContactEnabled
        {

            get { return saveContactEnabled; }
            set
            {
                saveContactEnabled = value;
                OnPropertyChanged();

            }

        }
        //Register new user

        public bool MessageIsVisible
        {

            get { return messageIsVisible; }
            set
            {
                messageIsVisible = value;
                OnPropertyChanged();
            }

        }
        async Task AddContact()
        {
            var result = NewContactDA.GetContactByPhoneNumber(PhoneNo);

            if( result != null)
            {
                Message = "Phone has already exist for contact " + result.FullName ;
                MessageIsVisible = true;
            }
            else
            {
               // IsBusy = true;

                ContactsObj.FullName = Name;
                ContactsObj.PhoneNumber = PhoneNo;
                NewContactDA.Insert(ContactsObj);

                //await Task.Delay(4000);
                //IsBusy = false;
                await _navigation.PushAsync(new ListContacts());
                
            }
        }
    }
}
