﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using Xamarin.Forms;
using Contact.Model;
using Contact.DAL;
using Android.Content.Res;
using Contact.View;

namespace Contact.ViewModel
{
    public class SignInUserViewModel : INotifyPropertyChanged
    {
        private INavigation _navigation;
        public SignInUserViewModel(INavigation navigation)
        {
            SignIn = new Command(async () => await Login(), () => !IsBusy);
            SignUp = new Command(async () => await Register());
            _navigation = navigation;
        }

        private string username;
        private string password;
        private bool isBusy;
        private bool signInEnabled = false;
        private bool messageIsVisible = false;
        public string message { get; set; }

        UserDA NewUserDA = new UserDA();

        public Command SignIn { get; }
        public Command SignUp { get; }


        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string Username
        {

            get { return username; }
            set
            {
                username = value;
                if (username != null && Password != null)
                {
                    SignInEnabled = true;
                }
                else
                {
                    SignInEnabled = false;
                }
                //Clear Message on Screen
                if (username != null || Password != null)
                {
                    Message = "";
                    MessageIsVisible = false;
                }

                OnPropertyChanged();
            }

        }

        public string Password
        {

            get { return password; }
            set
            {
                password = value;

                if (Username != null && password != null)
                {
                    SignInEnabled = true;
                }
                else
                {
                    SignInEnabled = false;
                }

                //Clear Message on Screen
                if (username != null || Password != null)
                {
                    Message = "";
                    MessageIsVisible = false;
                }
                OnPropertyChanged();
            }

        }

        public string Message
        {

            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged();
            }

        }


        public bool IsBusy
        {

            get { return isBusy; }
            set
            {
                isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
                SignIn.ChangeCanExecute();
            }

        }

        public bool MessageIsVisible
        {

            get { return messageIsVisible; }
            set
            {
                messageIsVisible = value;
                OnPropertyChanged(nameof(MessageIsVisible));
            }

        }

        public bool SignInEnabled
        {
            get { return signInEnabled; }
            set
            {
                signInEnabled = value;
                OnPropertyChanged(nameof(SignInEnabled));

            }
        }
        async Task Login()
        {
            bool result = NewUserDA.Login(Username, Password);
            if (result == true)
            {
                IsBusy = true;
                await Task.Delay(4000);
                await _navigation.PushAsync(new ListContacts());
                IsBusy = false;
                Message = "";
                MessageIsVisible = false;
            }
            else
            {
                Message = "Invalid login details";
                MessageIsVisible = true;
            }

        }
        public async Task Register()
        {
            await _navigation.PushAsync(new Register());
        }
    }
}
