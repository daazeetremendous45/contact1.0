﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contact.ViewModel;

using Xamarin.Forms;

namespace Contact.View
{
    public partial class SignIn : ContentPage
    {
        public SignIn()
        {
            InitializeComponent();
            BindingContext = new SignInUserViewModel(this.Navigation);
        }
    }
}
