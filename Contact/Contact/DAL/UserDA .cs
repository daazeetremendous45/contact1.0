﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contact.Model;
using Xamarin.Forms;

namespace Contact.DAL
{
    public class UserDA
    {
        private SQLiteConnection database;
        private static object collisionLock = new object();
        public ObservableCollection<User> User { get; set; }

        public UserDA()
        {
            database =
              DependencyService.Get<IDatabaseConnection>().
              DbConnection();
            database.CreateTable<User>();
            this.User =
              new ObservableCollection<User>(database.Table<User>());
        }

        public int Insert(User UserObj)
        {
            lock (collisionLock)
            {
                database.Insert(UserObj);
                return UserObj.UserId;
            }
        }

        public IEnumerable<User> ListAllContact()
        {
            lock (collisionLock)
            {
                return database.
                  Query<User>
                  ("SELECT * FROM User").AsEnumerable();
            }
        }

        public User GetUserByUsername(string Username)
        {
            lock (collisionLock)
            {
                return database.
                  Query<User>
                  ("SELECT * FROM User WHERE Username='" + Username + "'").FirstOrDefault();
            }
        }

        public int Update(User UserObj)
        {
            lock (collisionLock)
            {
                database.Update(UserObj);
                return UserObj.UserId;
            }
        }

        public bool Login(string Username, string Password)
        {
            bool IsLogin = false;
            lock (collisionLock)
            {
             var result=    database.
                  Query<User>
                  ("SELECT * FROM User WHERE Username='" + Username + "' AND Password='" + Password + "'").FirstOrDefault();
                if (result != null)
                    IsLogin = true;
            }
            return IsLogin;
        }
    }
}
