﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contact.Model;
using Xamarin.Forms;

namespace Contact.DAL
{
 public   class ContactDA
    {
        private SQLiteConnection database;
        private static object collisionLock = new object();
        public ObservableCollection<Contacts> Contacts { get; set; }

        public ContactDA()
        {
            database =
              DependencyService.Get<IDatabaseConnection>().
              DbConnection();
            database.CreateTable<Contacts>();
            this.Contacts =
              new ObservableCollection<Contacts>(database.Table<Contacts>());
        }

        public int Insert(Contacts ContactsObj)
        {
            lock (collisionLock)
            {
                    database.Insert(ContactsObj);
                    return ContactsObj.ContactID;
            }
        }

        public IEnumerable<Contacts> ListAllContact()
        {
            lock (collisionLock)
            {
                return database.
                  Query<Contacts>
                  ("SELECT * FROM Contacts").AsEnumerable();
            }
        }

        public Contacts GetContactByPhoneNumber(string number)
        {
            lock (collisionLock)
            {
                return database.
                  Query<Contacts>
                  ("SELECT * FROM Contacts WHERE PhoneNumber='"+ number +"'").FirstOrDefault();
            }
        }

        public int Update(Contacts ContactsObj)
        {
            lock (collisionLock)
            {
                database.Update(ContactsObj);
                return ContactsObj.ContactID;
            }
        }

       // public void DeleteCustomer(Contacts ContactsObj)
       public void DeleteCustomer(int Id)
        {
            //var id = ContactsObj.ContactID;           
                lock (collisionLock)
                {
                    database.Delete<Contacts>(Id);
                }
         
           // this.Contacts.Remove(ContactsObj);
           // return id;
        }
    }
}
