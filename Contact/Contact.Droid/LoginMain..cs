using Android.Content;
using System.Linq;
using System.Threading.Tasks;
using Android.Telephony;
using Xamarin.Forms;


using Uri = Android.Net.Uri;
using Contact;
using Contact.Droid;

[assembly: Dependency(typeof(LoginMain))]

namespace Contact.Droid
{
    public class LoginMain : ILogin
    {
        /// <summary>
        /// Dial the phone
        /// </summary>
        //public Task<bool> LoginAsync()
        public void LoginAsync()
        {
            var context = Forms.Context;
            //if (context != null)
            //{
                var intent = new Intent(new MainActivity(), typeof(MainAppActivity));
                context.StartActivity(intent);
                //return Task.FromResult(true);
           // }
            //return Task.FromResult(false);
        }
    }
}
