using SQLite;
using System.IO;
using Contact.Droid;
[assembly: Xamarin.Forms.Dependency(typeof(DatabaseConnection_Android))]

namespace Contact.Droid
{
    public class DatabaseConnection_Android : IDatabaseConnection
    {
        public SQLiteConnection DbConnection()
        {
            var dbName = "ContactDB.db3";
            var path = Path.Combine(System.Environment.
              GetFolderPath(System.Environment.
              SpecialFolder.Personal), dbName);
            return new SQLiteConnection(path);
        }
    }
}